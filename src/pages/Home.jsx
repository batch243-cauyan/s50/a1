import React from 'react';

import { Container } from 'react-bootstrap';
import Banner from '../components/Banner';
import CardCourse from '../components/CardCourse';
import Highlights from '../components/Highlights';


const Home = () => {
    return (
        <Container>
            <Banner/>
            <Highlights/>
            <CardCourse/>
        </Container>
      );
}
 
export default Home;