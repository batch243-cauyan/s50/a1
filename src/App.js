
import './App.css';
import Home from './pages/Home.jsx';
import { Fragment } from 'react';

import AppNavbar from './components/AppNavbar';



function App() {

  return (

      <Fragment>
        <AppNavbar/>
        <Home/>
     
      </Fragment>

  );
}


export default App;
