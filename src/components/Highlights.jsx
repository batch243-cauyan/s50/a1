
import {Row, Col, Button, CardGroup, Card} from "react-bootstrap"


const Highlights = () => {
    return ( 
        <Row className="my-3">
            {/* This is the first card */}
            <Col xs={12} md={4}> 
                <Card className="cardHighlight p-3">
                    
                    <Card.Body>
                    <Card.Title><h2> Learn From Home</h2></Card.Title>
                    <Card.Text>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit rerum est similique perspiciatis quis non ex velit? Reprehenderit amet eum sint tempore cupiditate harum, at unde. Autem rem sint assumenda.
                    </Card.Text>
                    
                    </Card.Body>
                </Card>
            </Col>
            {/* This is the second CARD */}
            <Col xs={12} md={4}> 
                <Card className="cardHighlight p-3">
                 
                    <Card.Body>
                    <Card.Title><h2>Study Now, Pay Later</h2></Card.Title>
                    <Card.Text>
                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam, dolores unde voluptas labore neque eum rem asperiores officiis voluptate saepe placeat temporibus vero culpa nihil accusantium sed laboriosam iste tenetur.
                    </Card.Text>
                    
                    </Card.Body>
                </Card>
            </Col>
            {/* This is the third card */}
            <Col xs={12} md={4}> 
                <Card className="cardHighlight p-3">
                 
                    <Card.Body>
                    <Card.Title><h2>Be part of our Community</h2></Card.Title>
                    <Card.Text>
                        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quibusdam, nemo dolores! Voluptate, delectus ullam, dicta neque autem consectetur minima illo magnam saepe ex enim inventore nostrum, eos assumenda! Molestias, et.
                    </Card.Text>
                    
                    </Card.Body>
                </Card>
            </Col>
        </Row>
      
    );
}
 
export default Highlights;