import React from 'react';
import { Navbar, Container, Nav} from 'react-bootstrap';


const AppNavbar = () => {
    return (
    <nav>
        <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand href="#home">Course-Booking</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="#home">Home</Nav.Link>
              <Nav.Link href="#link">About</Nav.Link>
              <Nav.Link href="#link">Contact Us</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </nav> 
     );
}
 
export default AppNavbar;

