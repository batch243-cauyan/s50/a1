import { useState } from "react";
import {Card, Button, Col, Row} from "react-bootstrap"
import React from 'react';
import { ReactDOM } from "react";



const CardCourse = () => {


    const [courses, setCourses] = useState([{
        courseName:"Sample Course",
        Description: "This is a sample course offering.",
        Price: "Php 40,000",
        id :1
       },
       {
        courseName:"Full Stack Course",
        Description: "Learn Full Stack Web Development.",
        Price: "Php 50,000",
        id :2
       }
       ])

    return ( 
        <Row>
        {courses.map((course)=>(
            <Col key={course.id}>
                <Card>
            
                <Card.Body>
                <Card.Title>{course.courseName}</Card.Title>
                <Card.Text>
                    Description:
                    <br/>
                    {course.Description}
                </Card.Text>
                <Card.Text>
                    {course.Price}
                </Card.Text>
                <Button variant="primary">Enroll</Button>
                </Card.Body>
                </Card>
            </Col>
        ))}
        </Row>
            
     );
}
 
export default CardCourse;