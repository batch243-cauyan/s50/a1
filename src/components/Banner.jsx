
import React from 'react';
import {Container, Button, Row ,  Col} from 'react-bootstrap';

const Banner = () => {
    return ( 
        <Row className='Banner'>
            <Col>
                <h1 >Zuitt Coding Bootcamp</h1>
                <p >Opportunities for everyone, everywhere.</p>
                <Button >Enroll now!</Button>
            </Col>
        </Row>
     );
}
 
export default Banner;
